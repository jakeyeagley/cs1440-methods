//
// Created by yeagols on 4/19/17.
//

#include "UserInterface.h"

void UserInterface::openFile(std::string fileName)
{
    file.open(fileName);
    if(!file.is_open())
        throw std::string("error opening file");
    while(file.good())
    {
        std::string line;
        std::getline(file,line);
        inputStream input;
        input.setInputStream(line);
        inputStreams.push_back(input);
    }
}

void UserInterface::setParameters()
{
    std::string key;
    int parameter;


//    std::cout << "to run DOSAnalyzer type in;\n TimeFrame and then a value"
//            "\n likelyThreshold and then a value"
//            "\n possibleThreshold and then a value"
//              << std::endl;

    std::cout << "would you like to run DOSAnaylzer or PortScanAnalyzer? "
              << "\ntype DOS for DOSAnaylzer or Port for PortScanAnalyzer"
              << std::endl;

    std::cin >> pick;
    if(pick == "DOS")
    {
        std::cout << "TimeFrame: ";
        std::cin >> parameter;
        std::cout << std::endl;
        std::pair<std::string,int> pair("TimeFrame",parameter);
        parameters.push_back(pair);
        std::cout << "likelyThreshold: ";
        std::cin >> parameter;
        std::cout << std::endl;
        std::pair<std::string,int> pair2("likelyThreshold",parameter);
        parameters.push_back(pair2);
        std::cout << "possibleThreshold: ";
        std::cin >> parameter;
        std::cout << std::endl;
        std::pair<std::string,int> pair3("possibleThreshold",parameter);
        parameters.push_back(pair3);
    }
    if(pick == "Port")
    {
        std::cout << "TimeFrame: ";
        std::cin >> parameter;
        std::cout << std::endl;
        std::pair<std::string,int> pair("TimeFrame",parameter);
        parameters.push_back(pair);
        std::cout << "likelyAttackPortCount: ";
        std::cin >> parameter;
        std::cout << std::endl;
        std::pair<std::string,int> pair2("likelyAttackPortCount",parameter);
        parameters.push_back(pair2);
        std::cout << "possibleAttackPortCount: ";
        std::cin >> parameter;
        std::cout << std::endl;
        std::pair<std::string,int> pair3("possibleAttackPortCount",parameter);
        parameters.push_back(pair3);
    }
}