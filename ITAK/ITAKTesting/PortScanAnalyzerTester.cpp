//
// Created by yeagols on 4/11/17.
//

#include "PortScanAnalyzerTester.h"
#include "../PortScanAnalyzer.h"

void PortScanAnalyzerTester::testSetConfiguration()
{
    PortScanAnalyzer portScanAnalyzer;

    Configuration configuration;
    std::vector<std::pair<std::string,int>> testVector;
    std::pair<std::string,int> testPair1;

    testPair1.first = "TimeFrame";
    testPair1.second = 100;
    testVector.push_back(testPair1);

    std::pair<std::string,int> testPair2;
    testPair2.first = "LikelyAttackMessageCount";
    testPair2.second = 4;
    testVector.push_back(testPair2);

    std::pair<std::string,int> testPair3;
    testPair3.first = "PossibleAttackMessageCount";
    testPair3.second = 7;
    testVector.push_back(testPair3);

    configuration.setConfiguration(testVector);
    portScanAnalyzer.setConfiguration(configuration);

    if(portScanAnalyzer.getConfiguration().getParameter("TimeFrame") != 100)
    {
        std::cout << "error in DOSAnalyzer.setConfiguration unexepcted value of: "
                  << portScanAnalyzer.getConfiguration().getParameter("TimeFrame")
                  << std::endl;
    }

    if(portScanAnalyzer.getConfiguration().getParameter("LikelyAttackMessageCount") != 4)
    {
        std::cout << "error in DOSAnalyzer.setConfiguration unexepcted value of: "
                  << portScanAnalyzer.getConfiguration().getParameter("LikelyAttackMessageCount")
                  << std::endl;
    }

    if(portScanAnalyzer.getConfiguration().getParameter("PossibleAttackMessageCount") != 7)
    {
        std::cout << "error in DOSAnalyzer.setConfiguration unexepcted value of: "
                  << portScanAnalyzer.getConfiguration().getParameter("PossibleAttackMessageCount")
                  << std::endl;
    }
}

void PortScanAnalyzerTester::testRun()
{
    PortScanAnalyzer portScanAnalyzer;

    Configuration configuration;
    std::vector<std::pair<std::string,int>> testVector;
    std::pair<std::string,int> testPair1;

    testPair1.first = "TimeFrame";
    testPair1.second = 100;
    testVector.push_back(testPair1);

    std::pair<std::string,int> testPair2;
    testPair2.first = "LikelyAttackMessageCount";
    testPair2.second = 4;
    testVector.push_back(testPair2);

    std::pair<std::string,int> testPair3;
    testPair3.first = "PossibleAttackMessageCount";
    testPair3.second = 7;
    testVector.push_back(testPair3);

    configuration.setConfiguration(testVector);
    portScanAnalyzer.setConfiguration(configuration);

    inputStream inputStream1 ,inputStream2, inputStream3;
    inputStream1.setInputStream("1296000,119.43.23.54,57414,80");
    inputStream2.setInputStream("1296000,141.8.67.235,42975,3389");
    inputStream3.setInputStream("1296000,119.43.23.54,62580,80");

    std::vector<inputStream> testVect;
    testVect.push_back(inputStream1);
    testVect.push_back(inputStream2);
    testVect.push_back(inputStream3);

    //portScanAnalyzer.run(testVect);
}