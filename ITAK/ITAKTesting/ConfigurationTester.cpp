//
// Created by yeagols on 4/11/17.
//

#include "ConfigurationTester.h"
#include "../Configuration.h"

void ConfigurationTester::testGetParameter()
{
    Configuration configuration;
    std::vector<std::pair<std::string,int>> testVector;
    std::pair<std::string,int> testPair1;

    testPair1.first = "test";
    testPair1.second = 0;
    testVector.push_back(testPair1);

    std::pair<std::string,int> testPair2;
    testPair2.first = "";
    testPair2.second = 0;
    testVector.push_back(testPair2);

    std::pair<std::string,int> testPair3;
    testPair3.first = "A large string";
    testPair3.second = 0;
    testVector.push_back(testPair3);

    configuration.setConfiguration(testVector);

    if(configuration.getParameter("test") != 0)
    {
        std::cout << "error in configuration.getParameter unexpected value of: "
                  << configuration.getParameter("test")
                  << std::endl;
    }

    if(configuration.getParameter("") != 0)
    {
        std::cout << "error in configuration.getParameter unexpected value of: "
                  << configuration.getParameter("")
                  << std::endl;
    }

    if(configuration.getParameter("A large string") != 0)
    {
        std::cout << "error in configuration.getParameter unexpected value of: "
                  << configuration.getParameter("A large string")
                  << std::endl;
    }


}