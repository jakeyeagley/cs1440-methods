//
// Created by yeagols on 4/11/17.
//

#include "inputStreamTester.h"
#include "../inputStream.h"

void inputStreamTester::testSetInputStream()
{
    inputStream inputStream1;
    inputStream1.setInputStream("1296000,119.43.23.54,57414,80");

    if(inputStream1.getTimeStamp() != 1296000)
    {
        std::cout << "error in inputStream.setInputStream unexpected value of: "
                  << inputStream1.getTimeStamp()
                  << std::endl;
    }

    if(inputStream1.getSrcAddress() != "119.43.23.54")
    {
        std::cout << "error in inputStream.setInputStream unexpected value of: "
                  << inputStream1.getSrcAddress()
                  << std::endl;
    }

    if(inputStream1.getDesPort() != "80")
    {
        std::cout << "error in inputStream.setInputStream unexpected value of: "
                  << inputStream1.getDesPort()
                  << std::endl;
    }

    if(inputStream1.getSrcPort() != "57414")
    {
        std::cout << "error in inputStream.setInputStream unexpected value of: "
                  << inputStream1.getSrcPort()
                  << std::endl;
    }
}