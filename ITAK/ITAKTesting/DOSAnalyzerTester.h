//
// Created by yeagols on 4/11/17.
//

#ifndef ITAK_DOSANALYZERTESTER_H
#define ITAK_DOSANALYZERTESTER_H


class DOSAnalyzerTester {
public:
    void testRun();
    void testSetConfiguration();
};


#endif //ITAK_DOSANALYZERTESTER_H
