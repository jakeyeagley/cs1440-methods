//
// Created by yeagols on 4/11/17.
//

#include "ResultSet.h"

#include "Utils.h"

void ResultSet::print(std::ostream& out) const
{
    for(auto i = 0; i < m_results.size() - 1; i++)
    {
        out << m_results[i].first << ":" << std::endl;
        for(auto j = 0; j < m_results[i].second.size(); j++)
        {
            out << "\t" << m_results[i].second[j] << std::endl;
        }
    }
}

void ResultSet::setKey(std::string key)
{
    std::vector<std::string> vect;
    std::pair<std::string,std::vector<std::string>> pair;
    pair.first = key;
    pair.second = vect;
    m_results.push_back(pair);
}

void ResultSet::addValue(std::string name, std::string value)
{
    for(auto i = 0; i < m_results.size() - 1; i++)
    {
        if(m_results[i].first == name)
        {
            m_results[i].second.push_back(value);
        }
    }
}

void ResultSet::addValue(std::string name, unsigned int value)
{
    for(auto i = 0; i < m_results.size(); i++)
    {
        if(m_results[i].first == name)
        {
            m_results[i].second.push_back(std::to_string(value));
        }
    }
}