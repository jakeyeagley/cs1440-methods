//
// Created by yeagols on 4/11/17.
//

#ifndef ITAK_INPUTSTREAM_H
#define ITAK_INPUTSTREAM_H

#include "iostream"
#include "Utils.h"

class inputStream
{
private:
    unsigned int m_timeStamp;
    std::string m_srcAddress;
    std::string m_srcPort;
    std::string m_desPort;
public:
    void setInputStream(std::string);
    unsigned int getTimeStamp() { return m_timeStamp; }
    std::string getSrcAddress() { return m_srcAddress; }
    std::string getSrcPort() { return m_srcPort; }
    std::string getDesPort(){ return m_desPort; }
};


#endif //ITAK_INPUTSTREAM_H
