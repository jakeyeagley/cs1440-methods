//
// Created by yeagols on 3/27/17.
//

#ifndef GENERICDICTIONARY_DICTIONARY_H
#define GENERICDICTIONARY_DICTIONARY_H

#include <vector>
#include "KeyValue.h"
#include <string>
#include <iostream>

template <typename K, typename V>
class Dictionary {
private:
    std::vector<KeyValue<K,V>> m_KeyValues;
public:
    Dictionary() {m_KeyValues.reserve(10);}
    Dictionary(unsigned long int size) { m_KeyValues.reserve(size);};
    void add(K key,V value)
    {
        KeyValue<K,V> keyValue(key,value);
        m_KeyValues.push_back(keyValue);
    };
    KeyValue<K,V> getByKey(K key)
    {
        for(int i = 0; i < m_KeyValues.size(); i++)
        {
            if(key == m_KeyValues[i].getKey())
                return m_KeyValues[i];
        }
        throw std::string("out of range");
    };
    KeyValue<K,V> getByIndex(unsigned long index)
    {
        if(index >= 0 && index < m_KeyValues.size())
            return m_KeyValues[index];
        throw std::string("out of range");
    };
    void removeByKey(K key)
    {
        bool isErased = false;
        for(int i = 0; i < m_KeyValues.size(); i++)
        {
            if(key == m_KeyValues[i].getKey())
            {
                m_KeyValues.erase(m_KeyValues.begin() + i);
                isErased = true;
            }
                    }
        if(!isErased)
            throw std::string("out of range");
    };
    void removeByIndex(int index)
    {
        if(index >= 0 && index < m_KeyValues.size())
        {
            m_KeyValues.erase(m_KeyValues.begin() + index);
        }
        else
            throw std::string("out of range");
    };

    size_t getSize()
    {
        return m_KeyValues.size();
    }
};


#endif //GENERICDICTIONARY_DICTIONARY_H
