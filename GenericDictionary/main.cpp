#include <iostream>
#include "Dictionary.h"

int main() {
    std::cout << "dictionary 1: using an int for key and string for value" << std::endl;

    Dictionary<int, std::string> dictionary;
    dictionary.add(0,"zero");
    dictionary.add(1,"one");
    dictionary.add(2,"two");
    dictionary.add(3,"three");

    try {
        dictionary.getByIndex(4);
    }catch(std::out_of_range){ std::cout << "out of bounds" << std::endl;}
    try {
        dictionary.getByIndex(1);
    }catch(std::out_of_range){ std::cout << "out of bounds" << std::endl;}

    std::cout << "index 0: " << dictionary.getByIndex(0).getValue() << std::endl;
    std::cout << "index 1: " << dictionary.getByIndex(1).getValue() << std::endl;
    std::cout << "index 2: " << dictionary.getByIndex(2).getValue() << std::endl;
    std::cout << "index 3: " << dictionary.getByIndex(3).getValue() << std::endl;

    std::cout << std::endl;
    std::cout << "0: " << dictionary.getByKey(0).getValue() << std::endl;
    std::cout << "1: " << dictionary.getByKey(1).getValue() << std::endl;
    std::cout << "2: " << dictionary.getByKey(2).getValue() << std::endl;
    std::cout << "3: " << dictionary.getByKey(3).getValue() << std::endl;
    try {
        dictionary.removeByIndex(4);
    }catch(std::out_of_range){ std::cout << "out of bounds" << std::endl;}
    try {
        dictionary.removeByIndex(1);
    }catch(std::out_of_range){ std::cout << "out of bounds" << std::endl;}

    Dictionary<std::string, int> dictionary2;
    dictionary2.add("zero",0);
    dictionary2.add("one",1);
    dictionary2.add("two",2);
    dictionary2.add("three",3);

    std::cout << "dictionary 2: using a string for key and int for value" << std::endl;
    try{
        dictionary2.getByIndex(4);
    }catch(std::out_of_range){std::cout << "out of bounds" << std::endl;}
    try{
        dictionary2.getByIndex(1);
    }catch(std::out_of_range){std::cout << "out of bounds" << std::endl;}

    std::cout << "index 0: " << dictionary2.getByIndex(0).getValue() << std::endl;
    std::cout << "index 1: " << dictionary2.getByIndex(1).getValue() << std::endl;
    std::cout << "index 2: " << dictionary2.getByIndex(2).getValue() << std::endl;
    std::cout << "index 3: " << dictionary2.getByIndex(3).getValue() << std::endl;

    std::cout << std::endl;
    std::cout << "zero: " << dictionary2.getByKey("zero").getValue() << std::endl;
    std::cout << "one: " << dictionary2.getByKey("one").getValue() << std::endl;
    std::cout << "two: " << dictionary2.getByKey("two").getValue() << std::endl;
    std::cout << "three: " << dictionary2.getByKey("three").getValue() << std::endl;

    try {
        dictionary2.removeByIndex(4);
    }catch(std::out_of_range){ std::cout << "out of bounds" << std::endl;}
    try {
        dictionary2.removeByIndex(1);
    }catch(std::out_of_range){ std::cout << "out of bounds" << std::endl;}

    return 0;
}