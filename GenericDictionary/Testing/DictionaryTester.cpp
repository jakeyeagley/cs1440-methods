//
// Created by yeagols on 3/30/17.
//

#include "DictionaryTester.h"
#include "../Dictionary.h"

void DictionaryTester::testContructor()
{
    std::cout << "test constructor" << std::endl;
    Dictionary<int,int> dictionary;
    if(dictionary.getSize() != 0)
        std::cout << "error in testConstructor, unexpected value of " << dictionary.getSize() << std::endl;
}

void DictionaryTester::testContructor2()
{
    std::cout << "test constructor with parameters" << std::endl;
    Dictionary<int,int> dictionary(20);
    if(dictionary.getSize() != 0)
        std::cout << "error in testConstructor2, unexpected value of " << dictionary.getSize() << std::endl;
}

void DictionaryTester::testAdd()
{
    std::cout << "test add" << std::endl;
    Dictionary<int,int> dictionary;
    dictionary.add(5,10);
    if(dictionary.getByKey(5).getValue() != 10)
        std::cout << "error in testAdd, unexpected value of " << dictionary.getByKey(5).getValue() << std::endl;
    if(dictionary.getByKey(5).getKey() != 5)
        std::cout << "error in testAdd, unexpected value of " << dictionary.getByKey(5).getKey() << std::endl;
    if(dictionary.getByIndex(0).getValue() != 10)
        std::cout << "error in testAdd, unexpected value of " << dictionary.getByKey(5).getValue() << std::endl;
    if(dictionary.getByIndex(0).getKey() != 5)
        std::cout << "error in testAdd, unexpected value of " << dictionary.getByKey(5).getKey() << std::endl;
    try{
        dictionary.getByKey(5);
    }catch(const std::string &err){std::cout << "error should not have been thrown";};
    try{
        dictionary.getByKey(0);
        std::cout << "error should have been thrown" << std::endl;
    }catch(const std::string &err){};
}

void DictionaryTester::testRemoveByKey()
{
    std::cout << "test remove by key" << std::endl;
    Dictionary<int,std::string> dictionary;
    dictionary.add(1,"one");
    dictionary.add(2,"two");
    dictionary.add(3,"three");

    dictionary.removeByKey(1);
    try{
        dictionary.getByKey(1).getValue() == "one";
        std::cout << "error in restRemoveByKey, unexpected value of " << dictionary.getByKey(1).getValue() << std::endl;
    }catch (std::string &err) {}
    try{
        dictionary.removeByKey(2);
    }catch(const std::string &err){ std::cout << "error should not have been caught" << std::endl;}

    try{
        dictionary.removeByKey(2);
        std::cout << "error should have been thrown" << std::endl;
    }catch(const std::string &err){}
}

void DictionaryTester::testRemoveByIndex()
{
    std::cout << "test remove by index" << std::endl;
    Dictionary<int,int> dictionary;
    dictionary.add(1,2);
    dictionary.add(2,3);

    dictionary.removeByIndex(1);
    try{
        dictionary.getByIndex(1);
        std::cout << "error should not have been thrown" << std::endl;
    }catch(const std::string &err) {}

}