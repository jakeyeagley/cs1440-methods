//
// Created by yeagols on 3/30/17.
//

#ifndef GENERICDICTIONARY_DICTIONARYTESTER_H
#define GENERICDICTIONARY_DICTIONARYTESTER_H


class DictionaryTester {
public:
    void testContructor();
    void testContructor2();
    void testAdd();
    void testRemoveByKey();
    void testRemoveByIndex();
};


#endif //GENERICDICTIONARY_DICTIONARYTESTER_H
