//
// Created by yeagols on 3/30/17.
//

#ifndef GENERICDICTIONARY_KEYVALUETESTER_H
#define GENERICDICTIONARY_KEYVALUETESTER_H


class KeyValueTester {
public:
    void testConstructor();
    void testGetKey();
    void testGetValue();
};


#endif //GENERICDICTIONARY_KEYVALUETESTER_H
