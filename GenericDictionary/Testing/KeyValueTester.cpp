//
// Created by yeagols on 3/30/17.
//

#include "KeyValueTester.h"
#include "../KeyValue.h"


void KeyValueTester::testConstructor() {
    std::cout << "test constructor" << std::endl;
    KeyValue<int, int> keyValue1(1, 2);
    KeyValue<std::string, std::string> keyValue2("one", "two");
    KeyValue<int, std::string> keyValue3(1, "one");

    if (keyValue1.getKey() != 1) {
        std::cout << "error unexcpected value of " << keyValue1.getKey() << std::endl;
    }
    if (keyValue1.getValue() != 2)
    {
        std::cout << "error unexpected value of " << keyValue1.getValue() << std::endl;
    }
    if(keyValue2.getKey() != "one")
    {
        std::cout << "error unexpected value of " << keyValue2.getValue() << std::endl;
    }
    if(keyValue2.getValue() != "two")
    {
        std::cout << "error unexpected value of " << keyValue2.getValue() << std::endl;
    }
    if(keyValue3.getKey() != 1)
    {
        std::cout << "error unexpected value of " << keyValue3.getValue() << std::endl;
    }
    if(keyValue3.getValue() != "one")
    {
        std::cout << "error unexpected value of " << keyValue3.getValue() << std::endl;
    }
}

void KeyValueTester::testGetKey()
{
    std::cout << "test get key" << std::endl;
    KeyValue<int,int> keyValue(1,2);
    if(keyValue.getKey() != 1)
    {
        std::cout << "error unexpected value of " << keyValue.getValue() << std::endl;
    }
}

void KeyValueTester::testGetValue()
{
    std::cout << "test get value" << std::endl;
    KeyValue<int,int> keyValue(1,2);
    if(keyValue.getValue() != 2)
    {
        std::cout << "error unexpected value of " << keyValue.getValue() << std::endl;
    }
}