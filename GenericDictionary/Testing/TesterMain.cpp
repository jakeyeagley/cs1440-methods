//
// Created by yeagols on 3/30/17.
//

#include <iostream>
#include "DictionaryTester.h"
#include "KeyValueTester.h"

int main()
{
    std::cout << "test dictionary" << std::endl;
    DictionaryTester dictionaryTester;
    dictionaryTester.testContructor();
    dictionaryTester.testContructor2();
    dictionaryTester.testAdd();
    dictionaryTester.testRemoveByIndex();
    dictionaryTester.testRemoveByKey();

    std::cout << std::endl;
    std::cout << "test keyValue" << std::endl;
    KeyValueTester keyValueTester;
    keyValueTester.testConstructor();
    keyValueTester.testGetKey();
    keyValueTester.testGetValue();
  }
