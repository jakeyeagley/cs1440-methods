//
// Created by yeagols on 3/27/17.
//

#ifndef GENERICDICTIONARY_KEYVALUE_H
#define GENERICDICTIONARY_KEYVALUE_H

#include <iostream>

template <class K, class V>
class KeyValue {
private:
    K m_key;
    V m_value;
public:
    KeyValue(K key, V value) : m_key(key), m_value(value) {};
    K getKey() { return m_key;}
    V getValue() { return m_value; }
};


#endif //GENERICDICTIONARY_KEYVALUE_H
