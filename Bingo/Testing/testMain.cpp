//
// Created by Stephen Clyde on 2/20/17.
//

#include <iostream>
#include <ctime>

#include "MenuOptionTester.h"
#include "MenuTester.h"
#include "cardTester.h"
#include "deckTester.h"

int main()
{

    // Initialize the random number generator
    unsigned int seed = (unsigned int) time(NULL);
    srand(seed);

    // TODO: Test your components
    cardTester cardTester1;
    cardTester1.testConstructor();
    cardTester1.testPrint();

    deckTester deckTester1;
    deckTester1.testConstructor();
    deckTester1.testPrint();

    MenuOptionTester menuOptionTester;
    menuOptionTester.testConstructorAndGetter();

    MenuTester menuTester;
    menuTester.testConstructorAndGetter();
}