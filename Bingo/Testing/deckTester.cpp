//
// Created by Jake Yeagley on 2/28/17.
//

#include <iostream>
#include <fstream>
#include "deckTester.h"
#include "../Deck.h"

void deckTester::testConstructor() {
    std::cout << "Test Suite: deckTester::testConstructor " << std::endl;
    std::cout << "Test Case 1" << std::endl;
    Deck deck1(3, 5, 18);
    if (!deck1.get_is_valid())
    {
        std::cout << "Failure in constructor deck1: expected to be valid" << std::endl;
    }
    if (deck1.get_cardCount() != 5)
    {
        std::cout << "Failure in constructor deck1: unexpected value for cardCount of " << deck1.get_cardCount() << std::endl;
    }

    Deck deck2(2,5,8);
    if(deck2.get_is_valid())
    {
        std::cout << "Failure in constructor deck2: expected to not be valid" << std::endl;
    }

    Deck deck3(3,5,7);
    if(deck3.get_is_valid())
    {
        std::cout << "Failure in constructor deck3: expected to not be valid" << std::endl;
    }

    Deck deck4(3,0,8);
    if(deck4.get_is_valid())
    {
        std::cout << "Failure in constructor deck4: expected to not be valid" << std::endl;
    }
}

void deckTester::testPrint()
{
    std::cout << "Test Suite: deckTester::testPrint" << std::endl;
    Deck deck1(3,2,18);

    std::cout << "Testing print(std::cout)" << std::endl;
    deck1.print(std::cout);

    std::cout << "Testing print(std::cout,1)" << std::endl;
    deck1.print(std::cout,1);

    std::cout << "Testing print(std::cout,2)" << std::endl;
    deck1.print(std::cout,2);

    std::ofstream outputStream("test");

    std::cout << "Testing print(outputStream)" << std::endl;
    deck1.print(outputStream);

    std::cout << "Testing print(outputStream,1)" << std::endl;
    deck1.print(outputStream,1);

    std::cout << "Testing print(outputStream,2)" << std::endl;
    deck1.print(outputStream,2);



}