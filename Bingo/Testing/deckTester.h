//
// Created by Jake Yeagley on 2/28/17.
//

#ifndef BINGO_DECKTESTER_H
#define BINGO_DECKTESTER_H


class deckTester {
public:
    void testConstructor();
    void testPrint();
};


#endif //BINGO_DECKTESTER_H
