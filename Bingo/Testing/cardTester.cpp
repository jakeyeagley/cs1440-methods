//
// Created by Jake Yeagley on 2/22/17.
//

#include <iostream>
#include <fstream>
#include "cardTester.h"
#include "../Card.h"
#include "../Card.cpp"

void cardTester::testConstructor()
{
    std::cout << "Test Suite: cardTester::testConstructor " << std::endl;
    std::cout << "Test Case 1" << std::endl;

    Card card1(3,18);
    if(!card1.get_is_valid())
    {
        std::cout << "Failure in constructor card1: expected to be valid" << std::endl;
        return;
    }
    if(card1.get_size() != 3)
    {
        std::cout << "Failure in constructor card1: unexpected value for size of " << card1.get_size() << std::endl;
        return;
    }
    if(card1.get_max_num() != 18)
    {
        std::cout << "Failure in constructor card1: unexpected value for max num of " << card1.get_max_num() << std::endl;
        return;
    }
    if(card1.get_index() != 1)
    {
        std::cout << "Failure in constructor card1: unexpected value of index " << card1.get_index() << std::endl;
        return;
    }
    std::vector<std::vector<int>> test_vector1 = card1.get_spots();
    std::vector<int> test_dup1(card1.get_size() * card1.get_size());
    unsigned int count = 0;
    for(unsigned int i = 0; i < card1.get_size(); i++)
    {
        for(unsigned int j = 0; j < card1.get_size(); j++)
        {
            test_dup1[count] = test_vector1[i][j];
            count++;
        }
    }
    std::sort(test_dup1.begin(),test_dup1.end());
    auto duplicate = std::unique(test_dup1.begin(),test_dup1.end());
    if(duplicate != test_dup1.end())
    {
        std::cout << "Failure in constructor card1: duplicate number " << std::endl;
        return;
    }

    std::cout << std::endl;
}

void cardTester::testPrint()
{
    std::cout << "Test Suite: cardTester::testPrint" << std::endl;
    std::cout << "Test Case 1" << std::endl;
    std::cout << "Testing print(std::cout)" << std::endl;
    Card card1(3,18);
    card1.print(std::cout);

    std::cout << std::endl;

    std::cout << "Testing print(ofstream out)" << std::endl;
    std::ofstream outputStream("test");;
    card1.print(outputStream);

    std::cout << std::endl;
}