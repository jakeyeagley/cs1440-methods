//
// Created by Jake Yeagley on 2/22/17.
//
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include "Card.h"

int Card::next_index = 1;
Card::Card(unsigned long m_size, unsigned int m_max_num):size(m_size), max_num(m_max_num), spots(m_size,std::vector<int>(m_size)), is_valid(true)
{
    if(size < 3 || size > 15)
    {
        is_valid = false;
        return;
    }
    if((2 * size * size) > m_max_num || (4 * size * size) < m_max_num)
    {
        is_valid = false;
        return;
    }

    //an array to store numbers to be shuffled
    int numbers[size * size];
    for(int i = 0; i < (size * size); i++)
    {
        numbers[i] = i;
    }

    std::random_shuffle(&numbers[0],&numbers[size * size]);
    int count = 0;
    for(int i = 0; i < size; i++)
    {
        for(int j = 0; j < size; j++)
        {
            spots[i][j] = numbers[count];
            count++;
        }
    }
    index = next_index++;
}
void Card::print(std::ostream& out)
{
    for(int i = 0; i < size; i++)
    {
        for(int j = 0; j < size; j++)
        {
            out << "\t" << spots[i][j] << "\t";
        }
        out << std::endl;
    }
}