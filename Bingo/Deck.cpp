//
// Created by Stephen Clyde on 2/16/17.
//

#include <iostream>
#include "Deck.h"

Deck::Deck(unsigned int cardSize, unsigned int m_cardCount, unsigned int numberMax):cardCount(m_cardCount),is_valid(true)
{
    if(cardCount == 0)
        is_valid = false;
    for(unsigned int i = 0; i < cardCount; i++)
    {
        Card card(cardSize,numberMax);
        cards.push_back(card);
        if(!cards[i].get_is_valid())
        {
            is_valid = false;
            return;
        }
    }
}

Deck::~Deck()
{
    // TODO: Implement
}

void Deck::print(std::ostream& out)
{
    for(unsigned int i = 0; i < cards.size(); i++)
    {
        out << "card number " << i + 1 << std::endl;
        cards[i].print(out);
    }
    out << std::endl;
}

void Deck::print(std::ostream& out, int cardIndex)
{
    out << "card number " << cardIndex << std::endl;
    cards[cardIndex - 1].print(out);
    out << std::endl;
}



