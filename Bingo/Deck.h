//
// Created by Stephen Clyde on 2/16/17.
//

#ifndef BINGO_DECK_H
#define BINGO_DECK_H

#include <ostream>
#include "Card.h"


class Deck {
private:
    std::vector<Card> cards;
    bool is_valid;
    unsigned int cardCount;

public:
    Deck(unsigned int cardSize, unsigned int m_cardCount, unsigned int numberMax);
    ~Deck();

    void print(std::ostream& out);
    void print(std::ostream& out, int cardIndex);
    bool get_is_valid(){return is_valid;}
    unsigned int get_cardCount() {return cardCount;}
};

#endif //BINGO_DECK_H
