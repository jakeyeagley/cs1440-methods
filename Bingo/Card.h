//
// Created by Jake Yeagley on 2/22/17.
//

#ifndef BINGO_CARD_H
#define BINGO_CARD_H


#include <vector>

class Card {
private:
    unsigned long size;
    unsigned int max_num;
    std::vector<std::vector<int>> spots;
    bool is_valid;
    unsigned int index;

public:
    Card(unsigned long m_size, unsigned int m_max_num);
    static int next_index;
    void print(std::ostream& out);
    unsigned long get_size(){return size;}
    unsigned int get_max_num() {return max_num;}
    bool get_is_valid() {return is_valid;}
    unsigned int get_index() { return index; }
    std::vector<std::vector<int>> get_spots() {return spots;}
};


#endif //BINGO_CARD_H
