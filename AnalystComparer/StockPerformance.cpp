//
// Created by Jake Yeagley on 1/20/17.
//

#include <iostream>
#include "StockPerformance.h"

StockPerformance::StockPerformance() {
    SPL = 0;
    minPT = 0;
    maxST = 0;
    SID = 0;
    SPLPD = 0;

}
void StockPerformance::setSPL() {
    for(int i = 0; i < stock.size(); i++){
        SPL += stock.at(i).getPL();
    }
}

void StockPerformance::setMaxST() {
    maxST = 0.0;
    for(int i = 0; i < stock.size(); i++){
        if(stock.at(i).getST() > maxST)
            maxST = stock.at(i).getST();
    }
}
void StockPerformance::setMinPT() {
    minPT = 1000000000.0;
    for(int i = 0; i < stock.size(); i++){
        if(stock.at(i).getPT() < minPT)
            minPT = stock.at(i).getPT();
    }
}
void StockPerformance::calculateSPLPD() {
    setSPL();
    setMaxST();
    setMinPT();
    calculateSID();

    SPLPD = SPL / SID;
}