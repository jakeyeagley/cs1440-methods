#include <fstream>
#include <iostream>
#include "Analyst.h"
#include "FormattedOutput.h"

int main(int argc, char* argv[])
{
    //takes in files
    std::ifstream file[9];
    for(int i = 0; i < argc - 1; i++)
        //load them in
        file[i].open(argv[i + 1]);

    //create analysts
    Analyst analyst1(file[0]);
    Analyst analyst2(file[1]);
    Analyst analyst3(file[2]);

    //output to file
    if(analyst3.getName() == "") {
        std::ofstream output("output.txt");
        FormattedOutput formattedOutput(analyst1, analyst2);
        formattedOutput.writeToFile(output);
    }
    else {
        std::ofstream output2("output.txt");
        FormattedOutput formattedOutput1(analyst1, analyst2, analyst3);
        formattedOutput1.writeToFile(output2);
    }

    //check file output.txt in data folder to see results
}
