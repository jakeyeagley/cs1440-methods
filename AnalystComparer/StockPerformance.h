//
// Created by Jake Yeagley on 1/20/17.
//

#ifndef ANALYSTCOMPARER_STOCKPERFORMANCE_H
#define ANALYSTCOMPARER_STOCKPERFORMANCE_H

#include <string>
#include <vector>
#include "PurchaseSale.h"

class StockPerformance {
    std::string symbol;
    std::vector<PurchaseSale> stock;
    int SPL;                                //stocks profit loss
    int minPT;                              //minimum purchase time
    int maxST;                              //maximus stock time
    float SID;                              //stock investment
    float SPLPD;                            //stock profit loss per day

public:
    StockPerformance();
    void setSPL();
    void setStock(PurchaseSale purchaseSale) { stock.push_back(purchaseSale); }
    void setMinPT();
    void setMaxST();
    void calculateSID() { SID = (maxST - minPT) / ( 24 * 60);}
    void calculateSPLPD();
    float getSPLPD(){ return SPLPD; }

};


#endif //ANALYSTCOMPARER_STOCKPERFORMANCE_H
