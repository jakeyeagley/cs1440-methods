//
// Created by Jake Yeagley on 1/27/17.
//

#include <iomanip>
#include "FormattedOutput.h"

FormattedOutput::FormattedOutput(Analyst a1 , Analyst a2) {
    analyst1 = a1;
    analyst2 = a2;
}
FormattedOutput::FormattedOutput(Analyst a1 , Analyst a2, Analyst a3) {
    analyst1 = a1;
    analyst2 = a2;
    analyst3 = a3;
}

void FormattedOutput::writeToFile(std::ofstream &outputFile) {
    if(analyst3.getName() == "") {
        outputFile << "Investors: " << std::endl << "\t " << analyst1.getName() << std::endl << "\t "
                   << analyst2.getName() << std::endl << std::endl;
        outputFile << "Overall Performance: " << std::endl << "\t\t\t | \t" << analyst1.getInitials() << "\t\t | \t"
                   << analyst2.getInitials() << "\t\t|" << std::endl;
        outputFile << "\t\t\t |-----------|----------| " << std::endl;
        outputFile << "days: " << "\t\t | \t " << analyst1.getDays() << "\t | \t " << analyst2.getDays() << " \t| "
                   << std::endl;
        outputFile << "seed: " << "\t\t | \t " << analyst1.getSeed() << "\t | \t" << analyst2.getSeed() << " \t| "
                   << std::endl;
        outputFile << "TPL: " << "\t\t | \t " << std::fixed << std::setprecision(2) << analyst1.getTPL() << " | "
                   << std::fixed << std::setprecision(2) << analyst2.getTPL() << "  | " << std::endl;
        outputFile << "PLPD: " << "\t\t | \t " << std::fixed << std::setprecision(2) << analyst1.getPLPD() << "\t |\t "
                   << std::fixed << std::setprecision(2) << analyst2.getPLPD() << " \t|" << std::endl << std::endl
                   << std::endl;
        outputFile << "Stock Performance: " << std::endl << "\t\t\t | \t" << analyst1.getInitials() << "\t\t | \t"
                   << analyst2.getInitials() << "\t\t|" << std::endl;
        outputFile << "\t\t\t |-----------|----------| " << std::endl;
        outputFile << "AMZN: " << "\t\t |\t " << std::fixed << std::setprecision(2) << analyst1.getAMZNStockPerf()
                   << "\t | \t" << std::fixed << std::setprecision(2) << analyst2.getAMZNStockPerf() << " \t|"
                   << std::endl;
        outputFile << "AAPL: " << "\t\t |\t " << std::fixed << std::setprecision(2) << analyst1.getAAPLStockPerf()
                   << "\t | \t" << std::fixed << std::setprecision(2) << analyst2.getAAPLStockPerf() << " \t|"
                   << std::endl;
        outputFile << "MSFT: " << "\t\t |\t " << std::fixed << std::setprecision(2) << analyst1.getMSFTStockPerf()
                   << "\t | \t" << std::fixed << std::setprecision(2) << analyst2.getMSFTStockPerf() << " \t|"
                   << std::endl;
        outputFile << "GOOGL: " << "\t\t |\t " << std::fixed << std::setprecision(2) << analyst1.getGOOGLStockPerf()
                   << "\t | \t" << std::fixed << std::setprecision(2) << analyst2.getGOOGLStockPerf() << " \t|"
                   << std::endl;
    }
    else{
        outputFile << "Investors: " << std::endl << "\t " << analyst1.getName() << std::endl << "\t "
                   << analyst2.getName() << std::endl << "\t " << analyst3.getName() << std::endl << std::endl;
        outputFile << "Overall Performance: " << std::endl << "\t\t\t | \t" << analyst1.getInitials() << "\t\t | \t"
                   << analyst2.getInitials() << "\t\t| \t" << analyst3.getInitials() << "\t\t| \t" << std::endl;
        outputFile << "\t\t\t |-----------|----------|-----------| " << std::endl;
        outputFile << "days: " << "\t\t | \t " << analyst1.getDays() << "\t | \t " << analyst2.getDays() << "\t|\t"
                   << analyst3.getDays() << " \t| \t " << std::endl;
        outputFile << "seed: " << "\t\t | \t " << analyst1.getSeed() << "\t | \t" << analyst2.getSeed() << " \t|\t"
                   << analyst3 .getSeed() << "\t|" << std::endl;
        outputFile << "TPL: " << "\t\t | \t " << std::fixed << std::setprecision(2) << analyst1.getTPL() << " | "
                   << std::fixed << std::setprecision(2) << analyst2.getTPL() << "\t| " << std::fixed << std::setprecision(2)
                   << analyst3.getTPL() << "\t|\t" << std::endl;
        outputFile << "PLPD: " << "\t\t | \t " << std::fixed << std::setprecision(2) << analyst1.getPLPD() << "\t |\t "
                   << std::fixed << std::setprecision(2) << analyst2.getPLPD() << "\t|\t" << std::fixed
                   << std::setprecision(2) << analyst3.getPLPD() << "\t|" << std::endl << std::endl
                   << std::endl;
        outputFile << "Stock Performance: " << std::endl << "\t\t\t | \t" << analyst1.getInitials() << "\t\t | \t"
                   << analyst2.getInitials() << "\t\t|\t" << analyst3.getInitials() << "\t\t|" << std::endl;
        outputFile << "\t\t\t |-----------|----------|-----------| " << std::endl;
        outputFile << "AMZN: " << "\t\t |\t " << std::fixed << std::setprecision(2) << analyst1.getAMZNStockPerf()
                   << "\t | \t" << std::fixed << std::setprecision(2) << analyst2.getAMZNStockPerf() << " \t|\t"
                   << std::fixed << std::setprecision(2) << analyst3.getAMZNStockPerf() << " \t|" << std::endl;
        outputFile << "AAPL: " << "\t\t |\t " << std::fixed << std::setprecision(2) << analyst1.getAAPLStockPerf()
                   << "\t | \t" << std::fixed << std::setprecision(2) << analyst2.getAAPLStockPerf() << " \t|\t"
                   << std::fixed << std::setprecision(2) << analyst3.getAAPLStockPerf() << " \t|" << std::endl;
        outputFile << "MSFT: " << "\t\t |\t " << std::fixed << std::setprecision(2) << analyst1.getMSFTStockPerf()
                   << "\t | \t" << std::fixed << std::setprecision(2) << analyst2.getMSFTStockPerf() << " \t|\t"
                   << std::fixed << std::setprecision(2) << analyst3.getMSFTStockPerf() << " \t|" << std::endl;
        outputFile << "GOOGL: " << "\t\t |\t " << std::fixed << std::setprecision(2) << analyst1.getGOOGLStockPerf()
                   << "\t | \t" << std::fixed << std::setprecision(2) << analyst2.getGOOGLStockPerf() << " \t|\t"
                   << std::fixed << std::setprecision(2) << analyst3.getMSFTStockPerf() << " \t|" << std::endl;
    }
}