//
// Created by Jake Yeagley on 1/20/17.
//

#ifndef ANALYSTCOMPARER_ANALYST_H
#define ANALYSTCOMPARER_ANALYST_H


#include <string>
#include <fstream>
#include "History.h"

class Analyst {
private:
    std::string     initials;
    std::string     nameF;
    std::string     nameM;
    std::string     nameL;
    History         anaHistory;

public:
    Analyst();
    Analyst(std::ifstream& file);
    float getAAPLStockPerf() { return anaHistory.getAAPLSPLPD(); }
    float getGOOGLStockPerf() { return anaHistory.getGOOGLSPLPD(); }
    float getAMZNStockPerf() { return anaHistory.getAMZNSPLPD(); }
    float getMSFTStockPerf() { return anaHistory.getMSFTSPLPD(); }
    std::string getName();
    std::string getInitials() { return initials; }
    int getDays(){ return anaHistory.getDays(); }
    int getSeed() { return anaHistory.getSeed(); }
    float getTPL() { return anaHistory.getTPL(); }
    float getPLPD() { return anaHistory.getPLPD(); }
};


#endif //ANALYSTCOMPARER_ANALYST_H
