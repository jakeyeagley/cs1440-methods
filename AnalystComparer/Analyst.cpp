//
// Created by Jake Yeagley on 1/20/17.
//

#include <iostream>
#include "Analyst.h"

Analyst::Analyst(std::ifstream& file){
    file >> nameF >> nameM >> nameL >> initials;
    //debug
//    std::cout << "Analyst " << nameF << " " << nameM << " " << nameL << std::endl;
    History history(file);
    anaHistory = history;
}
std::string Analyst::getName() {
    if(nameF == "")
        return "";
    else
        return nameF + " " + nameM + ". " + nameL;
}
Analyst::Analyst() {}

