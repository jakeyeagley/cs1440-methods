//
// Created by Jake Yeagley on 1/20/17.
//

#ifndef ANALYSTCOMPARER_OVERALLPERFORMACE_H
#define ANALYSTCOMPARER_OVERALLPERFORMACE_H


#include <vector>
#include <fstream>
#include "PurchaseSale.h"
#include "StockPerformance.h"

class History {
    int days;
    int seed;
    int num;
    std::vector<PurchaseSale>   PS;
    StockPerformance            AMZN;
    StockPerformance            AAPL;
    StockPerformance            MSFT;
    StockPerformance            GOOGL;
    float                       TPL;        //total profit loss
    float                       PLPD;       //total profit loss per day

public:
    History();
    History(std::ifstream& file);
    int getDays(){ return days; }
    int getSeed() { return seed; }
    void calculateTPL();
    void calculatePLPD(){ PLPD = TPL / days; };
    float getTPL() { return TPL; }
    float getPLPD() { return PLPD; }
    float getAMZNSPLPD() { return AMZN.getSPLPD(); }
    float getAAPLSPLPD() { return AAPL.getSPLPD(); }
    float getMSFTSPLPD() { return MSFT.getSPLPD(); }
    float getGOOGLSPLPD() { return GOOGL.getSPLPD(); }
};


#endif //ANALYSTCOMPARER_OVERALLPERFORMACE_H
