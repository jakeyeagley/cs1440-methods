//
// Created by Jake Yeagley on 1/20/17.
//

#include <iomanip>
#include "PurchaseSale.h"

PurchaseSale::PurchaseSale(std::ifstream& file) {
    file >> symbol >> Q >> PT >> PP >> PF >> ST >> SP >> SF;
    PP = PP / 100;
    PF = PF / 100;
    SP = SP / 100;
    SF = SF / 100;
    PL = 0;
    INV = 0;

    calculateInv();
    calculatePL();
}