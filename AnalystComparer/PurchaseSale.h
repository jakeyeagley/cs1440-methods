//
// Created by Jake Yeagley on 1/20/17.
//

#ifndef ANALYSTCOMPARER_INVESTMENT_H
#define ANALYSTCOMPARER_INVESTMENT_H


#include <string>
#include <fstream>
#include <cmath>

class PurchaseSale {
    std::string     symbol;
    float           Q;      //quality
    int             PT;     //profit date/time
    float           PP;     //purchase price
    float           PF;     //purchase trans fee
    int             ST;     //sale date/time
    float           SP;     //sale price
    float           SF;     //sale trans fee
    float           INV;    //investment
    float           PL;     //profit loss

public:
    PurchaseSale();
    PurchaseSale(std::ifstream& file);
    std::string getSymbol() { return symbol; }
    float getPL(){ return PL; }
    int getPT(){ return PT; }
    int getST(){ return ST; }
    void calculateInv() { INV = Q * PP + PF + SF;}
    void calculatePL(){ PL = Q * SP - INV;}
};


#endif //ANALYSTCOMPARER_INVESTMENT_H
