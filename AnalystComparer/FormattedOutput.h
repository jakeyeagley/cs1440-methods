//
// Created by Jake Yeagley on 1/27/17.
//

#ifndef ANALYSTCOMPARER_FORMATTEDOUTPUT_H
#define ANALYSTCOMPARER_FORMATTEDOUTPUT_H


#include "Analyst.h"

class FormattedOutput {
    Analyst         analyst1;
    Analyst         analyst2;
    Analyst         analyst3;

public:
    FormattedOutput();
    FormattedOutput(Analyst a1 , Analyst a2);
    FormattedOutput(Analyst a1 , Analyst a2, Analyst a3);
    void writeToFile(std::ofstream& outputFile);
};


#endif //ANALYSTCOMPARER_FORMATTEDOUTPUT_H
