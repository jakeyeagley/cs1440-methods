//
// Created by Jake Yeagley on 1/20/17.
//

#include <iostream>
#include <iomanip>
#include "History.h"
#include "PurchaseSale.h"

History::History() {
    days = 0;
    seed = 0;
    num = 0;
    TPL = 0;
    PLPD = 0;
}
History::History(std::ifstream& file) {
    file >> days >> seed >> num;
    seed = seed / 100;

//    debug
//    std::cout << "History: " << days << " " << seed << " " << num << " " << std::endl;

    while(!file.eof()) {
        PurchaseSale purchaseSale(file);

        PS.push_back(purchaseSale);
        if(purchaseSale.getSymbol() == "AMZN")
            AMZN.setStock(purchaseSale);
        if(purchaseSale.getSymbol() == "MSFT")
            MSFT.setStock(purchaseSale);
        if(purchaseSale.getSymbol() == "AAPL")
            AAPL.setStock(purchaseSale);
        if(purchaseSale.getSymbol() == "GOOGL")
            GOOGL.setStock(purchaseSale);
    }

    AMZN.calculateSPLPD();
//    debug
//    std::cout << "Stock's performance: " << std::endl << "AMZN: " << AMZN.getSPLPD() << std::endl;
    MSFT.calculateSPLPD();
//    debug
//    std::cout << "MSFT: " << MSFT.getSPLPD() << std::endl;
    AAPL.calculateSPLPD();
//    debug
//    std::cout << "AAPL: " << AAPL.getSPLPD() << std::endl;
    GOOGL.calculateSPLPD();
//    debug
//    std::cout << "GOOGL: " << GOOGL.getSPLPD() << std::endl;

    calculateTPL();
    calculatePLPD();
//    debug
//    std::cout << "TPL: " <<  TPL << std::endl << "PLPD: " << PLPD << std::endl;
}
void History::calculateTPL() {
    for(int i = 0; i < PS.size(); i++){
        TPL += PS[i].getPL();
    }
}
