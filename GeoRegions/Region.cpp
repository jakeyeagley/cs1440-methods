//
// Created by Stephen Clyde on 3/4/17.
//

#include "Region.h"
#include "Utils.h"
#include "World.h"
#include "Nation.h"
#include "State.h"
#include "County.h"
#include "City.h"

#include <iomanip>
#include <iostream>

const std::string regionDelimiter = "^^^";
const int TAB_SIZE = 4;
unsigned int Region::m_nextId = 0;
const int DEFAULT_SIZE = 10;

Region* Region::create(std::istream &in)
{
    Region* region = nullptr;
    std::string line;
    std::getline(in, line);
    if (line!="")
    {
        region = create(line);
        if (region!= nullptr)
            region->loadChildren(in);
    }
    return region;
}
Region* Region::create(const std::string& data)
{
    Region* region = nullptr;
    std::string regionData;
    unsigned long commaPos = (int) data.find(",");
    if (commaPos != std::string::npos)
    {
        std::string regionTypeStr = data.substr(0,commaPos);
        regionData = data.substr(commaPos+1);

        bool isValid;
        RegionType regionType = (RegionType) convertStringToInt(regionTypeStr, &isValid);

        if (isValid)
        {
            region = create(regionType, regionData);
        }

    }

    return region;
}

Region* Region::create(RegionType regionType, const std::string& data)
{
    Region* region = nullptr;
    std::string fields[3];
    if (split(data, ',', fields, 3)) {

        // Create the region based on type
        switch (regionType) {
            case WorldType:
                region = new World();
                break;
            case NationType:
                region = new Nation(fields);
                break;
            case StateType:
                region = new State(fields);
                break;
            case CountyType:
                region = new County(fields);
                break;
            case CityType:
                region = new City(fields);
                break;
            default:
                break;
        }

        // If the region isn't valid, git ride of it
        if (region != nullptr && !region->getIsValid()) {
            delete region;
            region = nullptr;
        }
    }

    return region;
}

std::string Region::regionLabel(RegionType regionType)
{
    std::string label = "Unknown";
    switch (regionType)
    {
        case Region::WorldType:
            label = "World";
            break;
        case Region::NationType:
            label = "Nation";
            break;
        case Region::StateType:
            label = "State";
            break;
        case Region::CountyType:
            label = "County";
            break;
        case Region::CityType:
            label = "City";
            break;
        default:
            break;
    }
    return label;
}

Region::Region() {}

Region::Region(RegionType type, const std::string data[]) :
        m_id(getNextId()), m_regionType(type), m_isValid(true)
{
    m_name = data[0];
    m_population = convertStringToUnsignedInt(data[1], &m_isValid);
    if (m_isValid)
        m_area = convertStringToDouble(data[2], &m_isValid);
}

Region::~Region()
{
    if (m_allocated!=0)
    {
        for (int i = 0; i < m_allocated; i++) {
            delete m_regions[i];
        }
        delete [] m_regions;
    }
}

std::string Region::getRegionLabel() const
{
    return regionLabel(getType());
}

unsigned int Region::computeTotalPopulation()
{
    unsigned int population = m_population;
    for(int i = 0; i < m_count; i++)
    {
        population += m_regions[i]->computeTotalPopulation();
    }

    return population;
}

void Region::list(std::ostream& out)
{
    out << std::endl;
    out << getName() << ":" << std::endl;

    for (int i=0; i<m_count; i++)
    {
        if (m_regions[i] != nullptr)
        {
            if (m_regions[i]->getIsValid())
                m_regions[i]->display(out, 0, false);
        }
    }
}

void Region::display(std::ostream& out, unsigned int displayLevel, bool showChild)
{
    if (displayLevel>0)
    {
        out << std::setw(displayLevel * TAB_SIZE) << " ";
    }

    unsigned totalPopulation = computeTotalPopulation();
    double area = getArea();
    double density = (double) totalPopulation / area;

    out << std::setw(6) << getId() << "  "
        << getName()
        << ", total population=" << totalPopulation
        <<", population="
        << m_population
        << ", area=" << area
        << ", density=" << density << std::endl;

    if (showChild)
    {
        // foreach subregion
        //      display that subregion at displayLevel+1 with the same showChild value
        for(int i = 0; i < m_count; i++)
        {
            m_regions[i]->display(out,++displayLevel, true);
            //in the of display call display for eash sub-region with displayLevel+1
            --displayLevel;
        }
    }
}

void Region::save(std::ostream& out)
{
    out << getType()
        << "," << getName()
        << "," << getPopulation()
        << "," << getArea()
        << std::endl;

    for(int i = 0; i < m_allocated; i++)
    {
        if (m_regions[i]!= nullptr)
        {
            if (m_regions[i]->getIsValid())
                m_regions[i]->save(out);
        }
    }

    out << regionDelimiter << std::endl;
}

void Region::validate()
{
    m_isValid = (m_area!=UnknownRegionType && m_name!="" && m_area>=0);
}

void Region::loadChildren(std::istream& in)
{
    std::string line;
    bool done = false;
    while (!in.eof() && !done)
    {
        std::getline(in, line);
        if (line==regionDelimiter)
        {
            done = true;
        }
        else
        {
            Region* child = create(line);
            if (child!= nullptr)
            {
                addRegion(child);
                child->loadChildren(in);
            }
        }
    }
}
void Region::addRegion(Region *region)
{
    if (region!= nullptr)
    {
        if (m_allocated == 0)
        {
            m_regions = new Region *[DEFAULT_SIZE];
            m_allocated = DEFAULT_SIZE;
        }

        if (m_count == m_allocated)
            grow();

        m_regions[m_count++] = region;
    }
}
unsigned int Region::getNextId()
{
    if (m_nextId==UINT32_MAX)
        m_nextId=1;

    return m_nextId++;
}

void Region::grow()
{
    m_allocated *= 2;
    Region** newRegion = new Region*[m_allocated];

    for (int i=0; i<m_allocated; i++)
    {
        newRegion[i] = m_regions[i];
    }

    delete [] m_regions;

    m_regions = newRegion;
}

void Region::remove(Region* region)
{
    bool isDeleted = false;
    for(int i = 0; i < m_count; i++)
    {
        if (m_regions[i]->getId() == region->getId())
        {
            delete m_regions[i];
            m_regions[i] = nullptr;
            m_count--;
            isDeleted = true;
        }
        if (isDeleted)
        {
            m_regions[i] = m_regions[i + 1];
        }
    }
}

Region* Region::getRegion(int index) const
{
    Region* result = nullptr;
    if (m_count>0 && index>=0 && index < m_count)
    {
        result = m_regions[index];
    }
    return result;
}

unsigned int Region::getSubRegionCount()
{
    return (unsigned int) m_count;
}

Region* Region::getSubRegionByIndex(unsigned int index)
{


    if(this->getId() == index)
        return this;
    for(int i = 0; i < m_count; i++)
    {
        if(m_regions[i]->getIsValid())
        {
            if (m_regions[i]->getId() == index)
                return m_regions[i];
            else
                m_regions[i]->getSubRegionByIndex(index);
        }
    }

    return nullptr;
}